package de.ntb.persodategrabber;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.net.URL;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
public class RestService {

    @GetMapping("/date")
    private ResponseEntity<String> run() throws Exception {
        //Instantiating the URL class
        URL url = new URL("https://www.service.bremen.de/dienstleistungen/personalausweis-beantragen-8363");
        //Retrieving the contents of the specified page
        Scanner sc = new Scanner(url.openStream());
        //Instantiating the StringBuffer class to hold the result
        StringBuilder sb = new StringBuilder();
        while (sc.hasNext()) {
            sb.append(sc.next());
        }
        //Retrieving the String from the String Buffer object
        String result = sb.toString();
        //Removing the HTML tags
        result = result.replaceAll("<[^>]*>", "");
        int indexStart = result.indexOf("FrühestmöglicherTermininBremen");
        int indexStop = result.substring(indexStart).indexOf("am");

        String result2 = result.substring(indexStart, indexStart + indexStop + 20);

        Pattern pattern = Pattern.compile("\\d\\d\\.\\d\\d\\.\\d\\d");
        Matcher matcher = pattern.matcher(result2);
        String date = "";
        if (matcher.find()) {
            date = matcher.group();
        }

        DateTimeFormatter dtf = DateTimeFormat.forPattern("dd.MM.yy");
        LocalDate localDate = dtf.parseLocalDate(date);
        if (localDate.isBefore(LocalDate.parse("01.07.2024", dtf))) {
            System.out.println(dtf.print(localDate));
            return ResponseEntity.status(HttpStatus.OK).body(dtf.print(localDate));
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(dtf.print(localDate));

    }

}
